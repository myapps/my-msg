package cn.myapps.msg.mail.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.myapps.msg.mail.entity.MailAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 邮件服务器配置
 * @Author: jeecg-boot
 * @Date:   2020-06-09
 * @Version: V1.0
 */
public interface MailAccountMapper extends BaseMapper<MailAccount> {

}
