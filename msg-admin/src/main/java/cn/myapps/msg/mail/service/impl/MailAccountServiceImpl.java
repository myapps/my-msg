package cn.myapps.msg.mail.service.impl;

import cn.myapps.msg.mail.entity.MailAccount;
import cn.myapps.msg.mail.mapper.MailAccountMapper;
import cn.myapps.msg.mail.service.IMailAccountService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 邮件服务器配置
 * @Author: nicholas
 * @Date:   2020-06-09
 * @Version: V1.0
 */
@Service
public class MailAccountServiceImpl extends ServiceImpl<MailAccountMapper, MailAccount> implements IMailAccountService {

    @Override
    public MailAccount findByKey(String key) {
        LambdaQueryWrapper<MailAccount> lambdaQuery = Wrappers.<MailAccount>lambdaQuery();
        lambdaQuery.eq(MailAccount::getAccountKey, key);

        return getOne(lambdaQuery);
    }

    @Override
    public MailAccount findByMsgFrom(String msgFrom) {
        LambdaQueryWrapper<MailAccount> lambdaQuery = Wrappers.<MailAccount>lambdaQuery();
        lambdaQuery.eq(MailAccount::getMsgFrom, msgFrom);

        return getOne(lambdaQuery);
    }
}
