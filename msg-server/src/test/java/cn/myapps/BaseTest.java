package cn.myapps;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class BaseTest {
	public String readFileContent(String path) throws IOException{
		Resource r = (Resource) new ClassPathResource(path);
		InputStream inputStream = r.getInputStream();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = -1;
		while ((i = inputStream.read()) != -1) {
			baos.write(i);
		}
		
		String json = baos.toString("UTF-8");
		
		return json;
	}
}
