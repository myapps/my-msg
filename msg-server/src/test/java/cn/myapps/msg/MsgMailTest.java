package cn.myapps.msg;

import cn.myapps.msg.enums.MsgTypeEnum;
import cn.myapps.msg.maker.IMsgMaker;
import cn.myapps.msg.maker.MsgMakerFactory;
import cn.myapps.msg.sender.IMsgSender;
import cn.myapps.msg.sender.MsgSenderFactory;
import cn.myapps.msg.vo.MsgCommon;
import cn.myapps.msg.vo.SendResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MsgMailTest {

	private int count=6;

	@Test
	public void testSend() {
		IMsgSender sender = MsgSenderFactory.getMsgSender(MsgTypeEnum.EMAIL.getCode());

		// 一般模式发送
		MsgCommon msg2 = new MsgCommon();
		msg2.setFrom("scm-qa@hoolinks.com");
		msg2.setTo("zhenzhi@hoolinks.com");
		msg2.setTitle("测试邮件主题" + count);
		msg2.setContent("内容: 这是我的测试邮件" + count);

		SendResult result2 = sender.send(msg2);
		assertTrue(result2.isSuccess());
	}

	@Test
	public void testSendByTemplate() {
		IMsgSender sender = MsgSenderFactory.getMsgSender(MsgTypeEnum.EMAIL.getCode());

		// 模板模式发送
		MsgCommon msg = new MsgCommon();
		msg.setFrom("scm-qa@hoolinks.com");
		msg.setTo("zhenzhi@hoolinks.com");
		msg.setTemplateCode("test");

		// 其他参数
		msg.getOtherParams().put("username", "nicholas");

		SendResult result = sender.sendByTemplate(msg);
		assertTrue(result.isSuccess());
	}
}
