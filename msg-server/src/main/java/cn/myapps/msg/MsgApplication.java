package cn.myapps.msg;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@EnableSwagger2
@ComponentScan(basePackages={"org.jeecg","cn.myapps", "com.hoolinks"})
//@ImportResource({"classpath*:dubbo-consumer.xml", "classpath*:dubbo-provider.xml"})
@SpringBootApplication
public class MsgApplication {

    public static void main(String[] args) throws UnknownHostException {
    	System.setProperty("spring.devtools.restart.enabled", "true");

        ConfigurableApplicationContext application = SpringApplication.run(MsgApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        
        String[] beanNames = application.getBeanNamesForAnnotation(Controller.class);
//        log.info("Bean Names: ");
//        for (String beanName : beanNames) {
//        	System.out.println(beanName);
//		}
        
        log.info("\n----------------------------------------------------------\n\t" +
                "Application MSG is running! Access URLs:\n\t" +
                "Controller Size: \t" + beanNames.length + "\n\t" +
                "Local: \t\thttp://localhost:" + port + path + "/\n\t" +
                "External: \thttp://" + ip + ":" + port + path + "/\n\t" +
                "swagger-ui: \thttp://" + ip + ":" + port + path + "/swagger-ui.html\n\t" +
                "Doc: \t\thttp://" + ip + ":" + port + path + "/doc.html\n" +
                "----------------------------------------------------------");
    }
}