package cn.myapps.msg.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 微信模板数据
 * @Author: jeecg-boot
 * @Date:   2020-06-12
 * @Version: V1.0
 */
@Getter
@Setter
public class WxTemplateData implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5171703603973207362L;

	private String name;

	private String value;

	private String color;

	public WxTemplateData(){}

	public WxTemplateData(String name, String value, String color) {
		this.name = name;
		this.value = value;
		this.color = color;
	}


}
