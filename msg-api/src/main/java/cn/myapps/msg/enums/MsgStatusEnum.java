package cn.myapps.msg.enums;

/**
 * 消息状态
 */
public enum MsgStatusEnum {
    UNSENED(0,"未发送"),
    SENED(1, "已发送"),
    UNREAED(2, "未读"), // 收件后默认状态
    REAED(3, "已读"),
    PROCESSED(4, "已处理"),
    ERROR(-1, "异常");

    private int code;
    private String name;

    MsgStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
