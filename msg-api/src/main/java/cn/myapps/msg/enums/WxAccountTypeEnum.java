package cn.myapps.msg.enums;

/**
 * 消息状态
 */
public enum WxAccountTypeEnum {
    MP("mp", "公众号"),
    MA("ma","小程序");

    private String code;
    private String name;

    WxAccountTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
