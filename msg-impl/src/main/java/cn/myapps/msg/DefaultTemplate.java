package cn.myapps.msg;

import cn.myapps.msg.vo.MsgTemplate;

public class DefaultTemplate implements MsgTemplate {
    private String title = "${title}";
    private String content = "${content}";

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String getTemplateCode() {
        return "default";
    }

    @Override
    public String getId() {
        return "-1";
    }
}
