package cn.myapps.msg.mail.service;

import cn.myapps.msg.mail.entity.MsgMail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 邮件信息
 * @Author: jeecg-boot
 * @Date:   2020-06-09
 * @Version: V1.0
 */
public interface IMsgMailService extends IService<MsgMail> {

}
