package cn.myapps.msg.mail.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.myapps.msg.mail.entity.MailTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 邮件发送模板
 * @Author: jeecg-boot
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public interface MailTemplateMapper extends BaseMapper<MailTemplate> {

}
