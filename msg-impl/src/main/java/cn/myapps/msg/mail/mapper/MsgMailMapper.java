package cn.myapps.msg.mail.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.myapps.msg.mail.entity.MsgMail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 邮件信息
 * @Author: jeecg-boot
 * @Date:   2020-06-09
 * @Version: V1.0
 */
public interface MsgMailMapper extends BaseMapper<MsgMail> {

}
