package cn.myapps.msg.mail.service.impl;

import cn.myapps.msg.mail.entity.MailTemplate;
import cn.myapps.msg.mail.mapper.MailTemplateMapper;
import cn.myapps.msg.mail.service.IMailTemplateService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 邮件发送模板
 * @Author: jeecg-boot
 * @Date:   2020-06-10
 * @Version: V1.0
 */
@Service
public class MailTemplateServiceImpl extends ServiceImpl<MailTemplateMapper, MailTemplate> implements IMailTemplateService {

    @Override
    public MailTemplate findByCode(String code) {
        LambdaQueryWrapper<MailTemplate> lambdaQuery = Wrappers.<MailTemplate>lambdaQuery();
        lambdaQuery.eq(MailTemplate::getTemplateCode, code);

        return getOne(lambdaQuery);
    }
}
