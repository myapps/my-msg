package cn.myapps.msg.mail.entity;

import cn.myapps.msg.enums.MsgStatusEnum;
import cn.myapps.msg.enums.MsgTypeEnum;
import cn.myapps.msg.vo.MsgCommon;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description: 邮件消息
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
@Data
@TableName("my_msg_mail")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="my_msg_mail对象", description="邮件消息")
public class MsgMail {
    
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private java.lang.Integer id;
	/**消息类名*/
	@Excel(name = "消息类型", width = 15)
    @ApiModelProperty(value = "消息类型")
	private java.lang.Integer msgType;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**邮件主题*/
	@Excel(name = "邮件主题", width = 15)
    @ApiModelProperty(value = "邮件主题")
	private java.lang.String title;
	/**抄送*/
	@Excel(name = "抄送", width = 15)
    @ApiModelProperty(value = "抄送")
	private java.lang.String cc;
	/**邮件文件, 支持多个以;号分隔*/
	@Excel(name = "邮件文件JSON", width = 15)
    @ApiModelProperty(value = "邮件文件JSON")
	private java.lang.String fileJson;
	/**邮件内容*/
	@Excel(name = "邮件内容", width = 15)
    @ApiModelProperty(value = "邮件内容")
	private java.lang.String content;
	/**发送者*/
	@Excel(name = "发送者", width = 15)
    @ApiModelProperty(value = "发送者")
	private java.lang.String msgFrom;
	/**接收者,可以多个以;号分隔*/
	@Excel(name = "接收者,可以多个以;号分隔", width = 15)
    @ApiModelProperty(value = "接收者,可以多个以;号分隔")
	private java.lang.String msgTo;
	/**消息状态: 0-未发送;1-已发送*/
	@Excel(name = "消息状态: 0-未发送;1-已发送", width = 15)
    @ApiModelProperty(value = "消息状态: 0-未发送;1-已发送")
	private java.lang.Integer msgStatus;
	/**业务编码*/
	@Excel(name = "业务编码", width = 15)
    @ApiModelProperty(value = "业务编码")
	private java.lang.String bizNo;
	/**扩展字段1*/
	@Excel(name = "扩展字段1", width = 15)
    @ApiModelProperty(value = "扩展字段1")
	private java.lang.String field1;
	/**扩展字段2*/
	@Excel(name = "扩展字段2", width = 15)
    @ApiModelProperty(value = "扩展字段2")
	private java.lang.String field2;
	/**邮件模板id*/
	@Excel(name = "邮件模板id", width = 15)
    @ApiModelProperty(value = "邮件模板id")
	private java.lang.String templateId;
	/**企业UID*/
	@Excel(name = "企业UID", width = 15)
	@ApiModelProperty(value = "企业UID")
	private java.lang.String companyUid;
	/**发送异常*/
	@Excel(name = "异常信息", width = 15)
	@ApiModelProperty(value = "异常信息")
	private java.lang.String error;

	public static List<MsgMail> buildList(MsgCommon msgCommon) {
		List<MsgMail> rtn = new ArrayList<>();
		if (msgCommon != null) {
			String to = msgCommon.getTo();
			if (StringUtils.isNotBlank(to)) {
				String[] tos = to.split(";");
				for (String msgTo : tos) {
					MsgMail mail = new MsgMail();
					mail.setBizNo(msgCommon.getBizNo());
					mail.setMsgFrom(msgCommon.getFrom());
					mail.setMsgTo(msgCommon.getTo());
					mail.setTitle(msgCommon.getTitle());
					mail.setContent(msgCommon.getContent());
					mail.setCompanyUid(msgCommon.getCompanyUid());
					mail.setMsgStatus(MsgStatusEnum.UNSENED.getCode());
					mail.setMsgType(MsgTypeEnum.EMAIL.getCode());
					// 扩展字段
					if (msgCommon.getOtherParams() != null && msgCommon.getOtherParams().size() >0) {
						mail.setField1(MapUtils.getString(msgCommon.getOtherParams(), "field1"));
						mail.setField2(MapUtils.getString(msgCommon.getOtherParams(), "field2"));
						mail.setFileJson(MapUtils.getString(msgCommon.getOtherParams(), "fileJson")); // JSON结构
					}

					mail.setCreateTime(new Date());

					rtn.add(mail);
				}
			}
		}

		return rtn;
	}

	public List<String> getTos() {
		List<String> tos = Lists.newArrayList();
		tos.add(getMsgTo());

		return tos;
	}
}
