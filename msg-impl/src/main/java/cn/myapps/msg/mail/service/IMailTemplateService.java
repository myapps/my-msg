package cn.myapps.msg.mail.service;

import cn.hutool.extra.mail.Mail;
import cn.myapps.msg.mail.entity.MailTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 邮件发送模板
 * @Author: jeecg-boot
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public interface IMailTemplateService extends IService<MailTemplate> {
    public MailTemplate findByCode(String code);
}
