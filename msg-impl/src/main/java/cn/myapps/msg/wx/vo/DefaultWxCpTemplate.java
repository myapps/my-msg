package cn.myapps.msg.wx.vo;

public class DefaultWxCpTemplate extends WxCpTemplate {
    public DefaultWxCpTemplate() {
        this.agentId="${agentId}";
        this.btnTxt="${btnTxt}";
        this.content="${content}";
        this.title="${title}";
        this.desc="${desc}";
        this.msgType="文本消息";
        this.picUrl="${picUrl}";
        this.url="${url}";
        this.templateCode="defaultWxCpTemplate";
    }
}
