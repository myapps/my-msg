package cn.myapps.msg.wx.service.impl;

import cn.myapps.msg.wx.entity.MsgWxCp;
import cn.myapps.msg.wx.mapper.MsgWxCpMapper;
import cn.myapps.msg.wx.service.IMsgWxCpService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 企业微信消息
 * @Author: jeecg-boot
 * @Date:   2020-06-18
 * @Version: V1.0
 */
@Service
public class MsgWxCpServiceImpl extends ServiceImpl<MsgWxCpMapper, MsgWxCp> implements IMsgWxCpService {

}
