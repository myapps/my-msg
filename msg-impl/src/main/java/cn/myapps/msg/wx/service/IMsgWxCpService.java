package cn.myapps.msg.wx.service;

import cn.myapps.msg.wx.entity.MsgWxCp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 企业微信消息
 * @Author: jeecg-boot
 * @Date:   2020-06-18
 * @Version: V1.0
 */
public interface IMsgWxCpService extends IService<MsgWxCp> {

}
