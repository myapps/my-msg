package cn.myapps.msg.wx.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import cn.myapps.msg.enums.MsgStatusEnum;
import cn.myapps.msg.enums.MsgTypeEnum;
import cn.myapps.msg.mail.entity.MsgMail;
import cn.myapps.msg.vo.MsgCommon;
import cn.myapps.msg.vo.WxTemplateData;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 微信模板消息
 * @Author: nicholas
 * @Date:   2020-06-12
 * @Version: V1.0
 */
@Data
@TableName("my_msg_mp_template")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="my_msg_mp_template对象", description="微信模板消息")
public class MsgMpTemplate {
    
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private java.lang.Integer id;
	/**消息类型*/
	@Excel(name = "消息类型", width = 15)
    @ApiModelProperty(value = "消息类型")
	private java.lang.Integer msgType;
	/**公众号消息模板ID*/
	@Excel(name = "公众号消息模板ID", width = 15)
    @ApiModelProperty(value = "公众号消息模板ID")
	private java.lang.String templateId;
	/**发送消息ID*/
	@Excel(name = "公众号消息ID", width = 15)
    @ApiModelProperty(value = "公众号消息ID")
	private java.lang.String msgId;
	/**消息链接*/
	@Excel(name = "消息链接", width = 15)
    @ApiModelProperty(value = "消息链接")
	private java.lang.String url;
	/**发送者*/
	@Excel(name = "发送者", width = 15)
    @ApiModelProperty(value = "发送者")
	private java.lang.String msgFrom;
	/**接收者*/
	@Excel(name = "接收者", width = 15)
    @ApiModelProperty(value = "接收者")
	private java.lang.String msgTo;
	/**关联小程序appId*/
	@Excel(name = "关联小程序appId", width = 15)
    @ApiModelProperty(value = "关联小程序appId")
	private java.lang.String maAppid;
	/**关联小程序页面路径*/
	@Excel(name = "关联小程序页面路径", width = 15)
    @ApiModelProperty(value = "关联小程序页面路径")
	private java.lang.String maPagePath;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**企业UID*/
	@Excel(name = "企业UID", width = 15)
    @ApiModelProperty(value = "企业UID")
	private java.lang.String companyUid;
	/**异常信息*/
	@Excel(name = "异常信息", width = 15)
    @ApiModelProperty(value = "异常信息")
	private java.lang.String error;
	/**消息状态*/
	@Excel(name = "消息状态", width = 15)
	@ApiModelProperty(value = "消息状态")
	private java.lang.Integer msgStatus;
	/**模板数据JSON*/
	@Excel(name = "模板数据JSON", width = 15)
	@ApiModelProperty(value = "模板数据JSON")
	private java.lang.String templateDataJson;

	/**
	 * 构建模板消息
	 *
	 * @param msgCommon
	 * @return
	 */
	public static MsgMpTemplate build(MsgCommon msgCommon) {
		MsgMpTemplate msg = new MsgMpTemplate();
		BeanUtils.copyProperties(msgCommon, msg, "otherParams");
		msg.setMsgFrom(msgCommon.getFrom());
		msg.setMsgTo(msgCommon.getTo());
		msg.setMsgStatus(MsgStatusEnum.UNSENED.getCode());
		msg.setMsgType(MsgTypeEnum.MP_TEMPLATE.getCode());
		msg.setTemplateId(msgCommon.getTemplateCode());

		if (msgCommon.getOtherParams().containsKey("templateDataList") &&
				msgCommon.getOtherParams().get("templateDataList") instanceof List){
			msg.setTemplateDataJson(JSON.toJSONString(msgCommon.getOtherParams().get("templateDataList")));
		}

		msg.setCreateTime(new Date());

		return msg;
	}
}
