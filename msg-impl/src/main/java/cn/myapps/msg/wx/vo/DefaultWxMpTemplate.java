package cn.myapps.msg.wx.vo;

public class DefaultWxMpTemplate extends WxMpTemplate {

    public DefaultWxMpTemplate(String templateCode){
        super(templateCode);

        this.templateUrl="${url}";
        this.maPagePath="${maPagePath}";
    }
}
