package cn.myapps.msg.wx.service.impl;

import cn.myapps.msg.wx.entity.MsgMpTemplate;
import cn.myapps.msg.wx.mapper.MsgMpTemplateMapper;
import cn.myapps.msg.wx.service.IMsgMpTemplateService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 微信模板消息
 * @Author: jeecg-boot
 * @Date:   2020-06-12
 * @Version: V1.0
 */
@Service
public class MsgMpTemplateServiceImpl extends ServiceImpl<MsgMpTemplateMapper, MsgMpTemplate> implements IMsgMpTemplateService {

}
