package cn.myapps.msg.wx.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.myapps.msg.wx.entity.MsgMpTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信模板消息
 * @Author: jeecg-boot
 * @Date:   2020-06-12
 * @Version: V1.0
 */
public interface MsgMpTemplateMapper extends BaseMapper<MsgMpTemplate> {

}
