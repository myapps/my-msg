package cn.myapps.msg.wx.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.myapps.msg.wx.entity.MsgWxCp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 企业微信消息
 * @Author: jeecg-boot
 * @Date:   2020-06-18
 * @Version: V1.0
 */
public interface MsgWxCpMapper extends BaseMapper<MsgWxCp> {

}
