package cn.myapps.msg.wx.service;

import cn.myapps.msg.wx.entity.WxCpApp;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 微信企业应用配置
 * @Author: jeecg-boot
 * @Date:   2020-06-18
 * @Version: V1.0
 */
public interface IWxCpAppService extends IService<WxCpApp> {

    WxCpApp findByAgentId(String agentId);

    WxCpApp findByAppName(String appName);
}
