package cn.myapps.msg.sender;

import cn.myapps.msg.enums.MsgTypeEnum;

/**
 * @Description: 消息发送器工厂类
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public class MsgSenderFactory {

    /**
     * 根据消息类型获取对应的消息发送器
     *
     * @return IMsgSender
     * @param msgType
     */
    public static IMsgSender getMsgSender(int msgType) {
        IMsgSender iMsgSender = null;
        switch (msgType) {
            case MsgTypeEnum.MP_TEMPLATE_CODE:
                iMsgSender = new WxMpTemplateMsgSender();
                break;
            case MsgTypeEnum.MA_TEMPLATE_CODE:
                //iMsgSender = new WxMaTemplateMsgSender();
                break;
            case MsgTypeEnum.MA_SUBSCRIBE_CODE:
                //iMsgSender = new WxMaSubscribeMsgSender();
                break;
            case MsgTypeEnum.KEFU_CODE:
                //iMsgSender = new WxKefuMsgSender();
                break;
            case MsgTypeEnum.KEFU_PRIORITY_CODE:
                //iMsgSender = new WxKefuPriorMsgSender();
                break;
            case MsgTypeEnum.WX_UNIFORM_MESSAGE_CODE:
                //iMsgSender = new WxUniformMsgSender();
                break;
            case MsgTypeEnum.ALI_YUN_CODE:
                //iMsgSender = new AliYunMsgSender();
                break;
            case MsgTypeEnum.TX_YUN_CODE:
                //iMsgSender = new TxYunMsgSender();
                break;
            case MsgTypeEnum.HW_YUN_CODE:
                //iMsgSender = new HwYunMsgSender();
                break;
            case MsgTypeEnum.YUN_PIAN_CODE:
                //iMsgSender = new YunPianMsgSender();
                break;
            case MsgTypeEnum.EMAIL_CODE:
                iMsgSender = new MailMsgSender();
                break;
            case MsgTypeEnum.WX_CP_CODE:
                iMsgSender = new WxCpMsgSender();
                break;
            case MsgTypeEnum.HTTP_CODE:
                //iMsgSender = new HttpMsgSender();
                break;
            case MsgTypeEnum.DING_CODE:
                //iMsgSender = new DingMsgSender();
                break;
            case MsgTypeEnum.BD_YUN_CODE:
                //iMsgSender = new BdYunMsgSender();
                break;
            case MsgTypeEnum.UP_YUN_CODE:
                //iMsgSender = new UpYunMsgSender();
                break;
            case MsgTypeEnum.QI_NIU_YUN_CODE:
               //iMsgSender = new QiNiuYunMsgSender();
                break;
            default:
                break;
        }

        return iMsgSender;
    }
}
