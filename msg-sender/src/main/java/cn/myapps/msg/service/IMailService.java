package cn.myapps.msg.service;

import cn.myapps.msg.vo.MailFile;

import java.util.List;

/**
 * 邮件收/发服务
 */
public interface IMailService {
    void sendSimpleMail(String fromMail, String toMail, String subject, String content);

    void sendHtmlMail(String fromMail, String toMail, String subject, String content);

    void sendAttachmentsMail(String fromMail, String toMail, String subject, String content, String filePath);

    void sendUriAttachmentsMail(String fromMail, String toMail, String subject, String content, List<MailFile> files);
}
