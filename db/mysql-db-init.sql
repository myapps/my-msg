/*
Navicat MySQL Data Transfer

Source Server         : 192.168.3.13
Source Server Version : 50726
Source Host           : 192.168.3.13:3306
Source Database       : wepush

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-06-08 22:36:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for my_ding_app
-- ----------------------------
DROP TABLE IF EXISTS `my_ding_app`;
CREATE TABLE `my_ding_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) DEFAULT NULL,
  `agent_id` varchar(255) DEFAULT NULL,
  `app_key` varchar(255) DEFAULT NULL,
  `app_secret` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_ding
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_ding`;
CREATE TABLE `my_msg_ding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `radio_type` varchar(255) DEFAULT NULL,
  `ding_msg_type` varchar(255) DEFAULT NULL,
  `agent_id` varchar(255) DEFAULT NULL,
  `web_hook` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_http
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_http`;
CREATE TABLE `my_msg_http` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `params` varchar(255) DEFAULT NULL,
  `headers` varchar(255) DEFAULT NULL,
  `cookies` varchar(255) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `body_type` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_kefu
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_kefu`;
CREATE TABLE `my_msg_kefu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `kefu_msg_type` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `describes` text,
  `url` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_kefu_priority
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_kefu_priority`;
CREATE TABLE `my_msg_kefu_priority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ma_appid` varchar(255) DEFAULT NULL,
  `ma_page_path` varchar(255) DEFAULT NULL,
  `kefu_msg_type` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `describes` text,
  `kefu_url` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_mail
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_mail`;
CREATE TABLE `my_msg_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `Cc` varchar(255) DEFAULT NULL,
  `files` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_ma_subscribe
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_ma_subscribe`;
CREATE TABLE `my_msg_ma_subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_ma_template
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_ma_template`;
CREATE TABLE `my_msg_ma_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `emphasis_keyword` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_mp_template
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_mp_template`;
CREATE TABLE `my_msg_mp_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ma_appid` varchar(255) DEFAULT NULL,
  `ma_page_path` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_sms
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_sms`;
CREATE TABLE `my_msg_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_wx_cp
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_wx_cp`;
CREATE TABLE `my_msg_wx_cp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `cp_msg_type` varchar(255) DEFAULT NULL,
  `agent_id` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `describes` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `btn_txt` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_wx_uniform
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_wx_uniform`;
CREATE TABLE `my_msg_wx_uniform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `mp_template_id` varchar(255) DEFAULT NULL,
  `ma_template_id` varchar(255) DEFAULT NULL,
  `mp_url` varchar(255) DEFAULT NULL,
  `ma_appid` varchar(255) DEFAULT NULL,
  `ma_page_path` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `emphasis_keyword` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_send_history
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_send_history`;
CREATE TABLE `my_msg_send_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) DEFAULT NULL,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `csv_file` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_template_data
-- ----------------------------
DROP TABLE IF EXISTS `my_template_data`;
CREATE TABLE `my_template_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_wx_account
-- ----------------------------
DROP TABLE IF EXISTS `my_wx_account`;
CREATE TABLE `my_wx_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_type` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `app_id` varchar(255) DEFAULT NULL,
  `app_secret` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `aes_key` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_wx_cp_app
-- ----------------------------
DROP TABLE IF EXISTS `my_wx_cp_app`;
CREATE TABLE `my_wx_cp_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corpId` varchar(255) DEFAULT NULL,
  `app_name` varchar(255) DEFAULT NULL,
  `agent_id` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `aes_key` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_wx_mp_user
-- ----------------------------
DROP TABLE IF EXISTS `my_wx_mp_user`;
CREATE TABLE `my_wx_mp_user` (
  `open_id` varchar(255) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `sex_desc` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `head_img_url` varchar(255) DEFAULT NULL,
  `subscribe_time` varchar(255) DEFAULT NULL,
  `union_id` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `subscribe_scene` varchar(255) DEFAULT NULL,
  `qr_scene` varchar(255) DEFAULT NULL,
  `qr_scene_str` varchar(255) DEFAULT NULL,
  `create_time` varchar(255) DEFAULT NULL,
  `modified_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`open_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
